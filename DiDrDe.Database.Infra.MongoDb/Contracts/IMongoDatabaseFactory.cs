﻿using MongoDB.Driver;

namespace DiDrDe.Database.Infra.MongoDb.Contracts
{
    public interface IMongoDatabaseFactory
    {
        IMongoDatabase Create(string connectionString);
    }
}