﻿using Autofac;
using DiDrDe.Database.Infra.MongoDb.Contracts;
using DiDrDe.Database.Infra.MongoDb.Factories;
using DiDrDe.Database.Infra.MongoDb.Mappers;
using DiDrDe.Database.Infra.MongoDb.Model;
using DiDrDe.DotNetTesteable.Application.Contracts;
using DiDrDe.DotNetTesteable.Domain;
using MongoDB.Driver;
using System;

namespace DiDrDe.Database.Infra.MongoDb.IoCC.Autofac
{
    public static class RegistrationExtensions
    {
        public static void RegisterMongoDb(
            this ContainerBuilder builder,
            Func<IComponentContext, MongoDbOptions> optionsRetriever)
        {
            builder
                .RegisterType<MongoDatabaseFactory>()
                .As<IMongoDatabaseFactory>()
                .SingleInstance();

            builder
                .Register(context =>
                {
                    var ctx = context.Resolve<IComponentContext>();
                    var mongoDbOptions = optionsRetriever.Invoke(ctx);
                    var connectionString = mongoDbOptions.ConnectionString;
                    var mongoDatabaseFactory = ctx.Resolve<IMongoDatabaseFactory>();
                    var mongoDatabase = mongoDatabaseFactory.Create(connectionString);
                    var bookingsMongoCollection = mongoDatabase.GetCollection<DatabaseBooking>("Bookings");
                    return bookingsMongoCollection;
                })
                .As<IMongoCollection<DatabaseBooking>>();

            builder
                .RegisterType<BookingToDatabaseBookingMapper>()
                .As<IMapper<Booking, DatabaseBooking>>()
                .SingleInstance();

            builder
                .RegisterType<DatabaseBookingToBookingMapper>()
                .As<IMapper<DatabaseBooking, Booking>>()
                .SingleInstance();

            builder
                .RegisterType<MongoBookingsDatabase>()
                .As<IDatabase<Booking>>()
                .InstancePerDependency();
        }
    }
}