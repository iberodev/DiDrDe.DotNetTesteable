﻿using DiDrDe.Database.Infra.MongoDb.Model;
using DiDrDe.DotNetTesteable.Application.Contracts;
using DiDrDe.DotNetTesteable.Domain;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiDrDe.Database.Infra.MongoDb
{
    public class MongoBookingsDatabase
        : IDatabase<Booking>
    {
        private readonly IMongoCollection<DatabaseBooking> _bookings;
        private readonly IMapper<DatabaseBooking, Booking> _bookingMapper;
        private readonly IMapper<Booking, DatabaseBooking> _databaseBookingMapper;

        public MongoBookingsDatabase(
            IMongoCollection<DatabaseBooking> bookings,
            IMapper<DatabaseBooking, Booking> bookingMapper,
            IMapper<Booking, DatabaseBooking> databaseBookingMapper)
        {
            _bookings = bookings;
            _bookingMapper = bookingMapper;
            _databaseBookingMapper = databaseBookingMapper;
        }

        public async Task<Booking> Get(Guid id)
        {
            var queryResult = await _bookings.FindAsync(x => x.Id == id);
            var databaseBooking = await queryResult.SingleOrDefaultAsync();
            var booking = _bookingMapper.Map(databaseBooking);
            return booking;
        }

        public async Task<IEnumerable<Booking>> GetAll()
        {
            var queryFilter = Builders<DatabaseBooking>.Filter.Empty;
            var sortBuilder = Builders<DatabaseBooking>.Sort;
            var sort = sortBuilder.Descending(x => x.Name);
            var findOptions =
                new FindOptions<DatabaseBooking>
                {
                    Sort = sort
                };
            var queryResult = await _bookings.FindAsync(queryFilter, findOptions);
            var databaseBookings = await queryResult.ToListAsync();
            var bookings = databaseBookings.Select(x => _bookingMapper.Map(x));
            return bookings;
        }

        public async Task Save(Booking booking)
        {
            var databaseBooking = _databaseBookingMapper.Map(booking);
            await _bookings.InsertOneAsync(databaseBooking);
        }
    }
}