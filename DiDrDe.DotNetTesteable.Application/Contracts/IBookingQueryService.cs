﻿using DiDrDe.DotNetTesteable.Application.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DiDrDe.DotNetTesteable.Application.Contracts
{
    public interface IBookingQueryService
    {
        Task<IEnumerable<Booking>> GetBookings();
        Task<Booking> GetBooking(Guid id);
    }
}