﻿using DiDrDe.DotNetTesteable.Domain;

namespace DiDrDe.DotNetTesteable.Application.Contracts
{
    public interface IDomainBookingFactory
    {
        Booking Create(Model.Booking booking);
    }
}