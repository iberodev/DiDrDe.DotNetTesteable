﻿using DiDrDe.DotNetTesteable.Application.Contracts;
using System;

namespace DiDrDe.DotNetTesteable.Application.Factories
{
    public class IdFactory
        : IIdFactory
    {
        public Guid Create()
        {
            var newId = Guid.NewGuid();
            return newId;
        }
    }
}