﻿using DiDrDe.DotNetTesteable.Application.Contracts;
using DiDrDe.DotNetTesteable.WebApi.Controllers;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.DotNetTesteable.WebApi.UnitTests.Controllers.BookingsControllerTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private BookingsController _sut;
            private IBookingCommandService _bookingsCommandService;
            private IBookingQueryService _bookingsQueryService;

            protected override void Given()
            {
                _bookingsCommandService = Mock.Of<IBookingCommandService>();
                _bookingsQueryService = Mock.Of<IBookingQueryService>();
            }

            protected override void When()
            {
                _sut = new BookingsController(_bookingsCommandService, _bookingsQueryService);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_A_Controller()
            {
                _sut.Should().BeAssignableTo<Controller>();
            }
        }
    }
}