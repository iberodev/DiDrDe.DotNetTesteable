﻿using Autofac;
using DiDrDe.DotNetTesteable.WebApiApp.IoCC.Autofac.Modules;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DiDrDe.DotNetTesteable.WebApiApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This is where things can be registered directly with autofac and runs after ConfigureServices, so it will override it
        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule<ApplicationServicesModule>();
            builder.RegisterModule<DatabaseModule>();
            builder.RegisterModule<FactoriesModule>();
            builder.RegisterModule<MappersModule>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}